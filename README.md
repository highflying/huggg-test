# Server built with NextJS

## Handles both front-end and back-end APIs.

To run:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## APIs

Brand details

http://localhost:3000/api/v1/brands/5a4e6d14-53d4-4583-bd6b-49f81b021d24

Brand's products

http://localhost:3000/api/v1/brands/5a4e6d14-53d4-4583-bd6b-49f81b021d24/products

Brand's stores

http://localhost:3000/api/v1/brands/5a4e6d14-53d4-4583-bd6b-49f81b021d24/stores

Product's stores

http://localhost:3000/api/v1/products/5a3fe6f7-7796-44ca-84fe-70d4f751527d/stores
