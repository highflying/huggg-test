import React from "react";
import useSWR from "swr";
import styles from "../../styles/Home.module.scss";
import Link from "next/link";

interface BrandProps {
  brandId: string;
}

export const fetcher = (url: string) => fetch(url).then((res) => res.json());

const Brand: React.FC<BrandProps> = ({ brandId }) => {
  const { data, error } = useSWR(`/api/v1/brands/${brandId}`, fetcher);

  if (error) return <div>Failed to load</div>;
  if (!data) return <div>Loading...</div>;

  return (
    <div className={styles.brand}>
      <h2>{data.name}</h2>
      <a href={data.website} target="_blank" rel="noreferrer">
        <img
          src={`https://cdn.huggg.me/brands/${data.logo}`}
          alt={data.name}
          className={styles.logo}
        />
      </a>
      <Link href={`/products/${brandId}`}>
        <a>View Products</a>
      </Link>
    </div>
  );
};

export default Brand;
