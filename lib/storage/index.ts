import data from "../../brands.json";
import uniq from "lodash/uniq";

export const getBrand = (brandId: string) => {
  return data.data.find((row) => row.id === brandId);
};

export const getBrandProducts = (brandId: string) => {
  const brand = data.data.find((row) => row.id === brandId);

  if (!brand) {
    return;
  }

  const productIds = [...brand.products, ...brand.consolidated_products];

  const products = productIds.map((productId) =>
    data.embedded.products.find((product) => product.id === productId)
  );

  return products;
};

export const getBrandStores = (brandId: string) => {
  const brand = data.data.find((row) => row.id === brandId);

  if (!brand) {
    return;
  }

  const stores = brand.stores.map((storeId) =>
    data.embedded.stores.find((store) => store.id === storeId)
  );

  return stores;
};

export const getProductStores = (productId: string) => {
  const brandsWithProduct = data.data.filter((row) =>
    row.products.includes(productId)
  );

  if (!brandsWithProduct) {
    return;
  }

  const storeIds: string[] = uniq(
    brandsWithProduct
      .map((brand) => brand.stores)
      .filter((storeIds) => !!storeIds)
      .flat()
  );

  const stores = storeIds.map((storeId) =>
    data.embedded.stores.find((store) => store.id === storeId)
  );

  return stores;
};
