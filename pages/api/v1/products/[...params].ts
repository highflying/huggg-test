import type { NextApiRequest, NextApiResponse } from "next";
import { getProductStores } from "../../../../lib/storage";

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  const { params } = req.query;

  if (params[1]) {
    if (params[1] === "stores") {
      const stores = getProductStores(params[0]);

      if (stores) {
        return res.status(200).json(stores);
      }
    }
  }
  return res.status(404);
}
