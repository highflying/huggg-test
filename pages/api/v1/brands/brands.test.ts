import handler from "./[...params]";
import { testApiHandler } from "next-test-api-route-handler";

describe("/api/v1/brands/<brand_id>", () => {
  it("Returns brand data", async () => {
    await testApiHandler({
      handler,
      paramsPatcher: (params) => {
        params.params = ["5a4e6d14-53d4-4583-bd6b-49f81b021d24"];
      },
      test: async ({ fetch }) => {
        const res = await fetch({
          method: "GET",
        });
        expect(res.status).toBe(200);

        expect(await res.text()).toMatchSnapshot();
      },
    });
  });

  it("Returns 404 on unknown brand_id", async () => {
    await testApiHandler({
      handler,
      paramsPatcher: (params) => {
        params.params = ["5a4e6d14-53d4-4583-bbbb-bbbbbbbbbbbb"];
      },
      test: async ({ fetch }) => {
        const res = await fetch({
          method: "GET",
        });
        expect(res.status).toBe(404);
      },
    });
  });

  it("Returns 400 on POST", async () => {
    await testApiHandler({
      handler,
      paramsPatcher: (params) => {
        params.params = ["5a4e6d14-53d4-4583-bbbb-bbbbbbbbbbbb"];
      },
      test: async ({ fetch }) => {
        const res = await fetch({
          method: "POST",
        });
        expect(res.status).toBe(400);
      },
    });
  });
});

describe("/api/v1/brands/<brand_id>/products", () => {
  it("Returns product data", async () => {
    await testApiHandler({
      handler,
      paramsPatcher: (params) => {
        params.params = ["5a4e6d14-53d4-4583-bd6b-49f81b021d24", "products"];
      },
      test: async ({ fetch }) => {
        const res = await fetch({
          method: "GET",
        });
        expect(res.status).toBe(200);

        expect(await res.text()).toMatchSnapshot();
      },
    });
  });

  it("Returns 404 on unknown brand_id", async () => {
    await testApiHandler({
      handler,
      paramsPatcher: (params) => {
        params.params = ["5a4e6d14-53d4-4583-bbbb-bbbbbbbbbbbb", "products"];
      },
      test: async ({ fetch }) => {
        const res = await fetch({
          method: "GET",
        });
        expect(res.status).toBe(404);
      },
    });
  });
});

describe("/api/v1/brands/<brand_id>/stores", () => {
  it("Returns product data", async () => {
    await testApiHandler({
      handler,
      paramsPatcher: (params) => {
        params.params = ["5a4e6d14-53d4-4583-bd6b-49f81b021d24", "stores"];
      },
      test: async ({ fetch }) => {
        const res = await fetch({
          method: "GET",
        });
        expect(res.status).toBe(200);

        expect(await res.text()).toMatchSnapshot();
      },
    });
  });

  it("Returns 404 on unknown brand_id", async () => {
    await testApiHandler({
      handler,
      paramsPatcher: (params) => {
        params.params = ["5a4e6d14-53d4-4583-bbbb-bbbbbbbbbbbb", "stores"];
      },
      test: async ({ fetch }) => {
        const res = await fetch({
          method: "GET",
        });
        expect(res.status).toBe(404);
      },
    });
  });
});
