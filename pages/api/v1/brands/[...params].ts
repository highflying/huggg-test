import type { NextApiRequest, NextApiResponse } from "next";
import {
  getBrand,
  getBrandProducts,
  getBrandStores,
} from "../../../../lib/storage";

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const { params } = req.query;

  if (req.method !== "GET") {
    res.status(400).end();
    return;
  }

  if (params[1]) {
    if (params[1] === "products") {
      const products = getBrandProducts(params[0]);

      if (products) {
        res.status(200).json(products);
        return;
      }
    } else if (params[1] === "stores") {
      const stores = getBrandStores(params[0]);

      if (stores) {
        res.status(200).json(stores);
        return;
      }
    }
    res.status(404).end();
    return;
  } else {
    const brand = getBrand(params[0]);

    if (!brand) {
      res.status(404).end();
      return;
    }

    res.status(200).json(brand);
  }
};

export default handler;
