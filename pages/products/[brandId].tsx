import type { NextPage } from "next";
import { fetcher } from "../../lib/components/Brand";
import styles from "../../styles/Home.module.scss";
import { useRouter } from "next/router";
import useSWR from "swr";
import Link from "next/link";

const ProductsPage: NextPage = () => {
  const router = useRouter();
  const { brandId } = router.query;

  const { data, error } = useSWR(`/api/v1/brands/${brandId}/products`, fetcher);

  if (error) return <div>Failed to load</div>;
  if (!data) return <div>Loading...</div>;

  return (
    <div className={styles.container}>
      <Link href="/">
        <a>Back to Brands</a>
      </Link>
      <h1>Products</h1>
      {data.map((product: any) => (
        <div key={product.id} className={styles.product}>
          <h2>{product.label}</h2>
          <img
            src={`https://cdn.huggg.me/brands/${product.image}`}
            alt={`Image of ${product.label}`}
          />
          {product.description}
        </div>
      ))}
    </div>
  );
};

export default ProductsPage;
