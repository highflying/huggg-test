import type { NextPage } from "next";
import Brand from "../lib/components/Brand";
import styles from "../styles/Home.module.scss";

const brandIds = [
  "5a4e6d14-53d4-4583-bd6b-49f81b021d24",
  "a715b837-f4fc-48ba-ba0a-7f53b6dc59c5",
  "69be9b8c-5b95-4792-a05c-652d2f15a62f",
  "d2ff763c-1c88-474d-9672-48b55a53dd3f",
];

const Home: NextPage = () => {
  return (
    <div className={styles.container}>
      <h1>Huggg Brands</h1>
      {brandIds.map((brandId) => (
        <Brand key={brandId} brandId={brandId} />
      ))}
    </div>
  );
};

export default Home;
